//
// Created by AURELIEN on 28/09/2020.
//

#include "Matrix.h"

Matrix::Matrix() {
    for(unsigned int i = 0; i < Matrix::SIZE; ++i) {
        // Add randomly generated values between 0 and 100
        for(unsigned int j = 0; j < Matrix::SIZE; ++j) {
            this->values[i][j] = static_cast <float> (std::rand()) / (static_cast <float> (RAND_MAX/100.0f));
        }
    }
}

Matrix Matrix::MultiplicationSequential(const Matrix &l, const Matrix &r) {
    Matrix result = Matrix();
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    for(unsigned int i = 0; i < Matrix::SIZE; ++i) {
        for(unsigned int j = 0; j < Matrix::SIZE; ++j) {
            result[i][j] = 0.0f;
            for(unsigned int k = 0; k < Matrix::SIZE; ++k) {
                result[i][j] += l[i][k] * r[k][j];
            }
        }
    }
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Sequential matrix multiplication took \t\t" << std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count() << " nanoseconds\n";
    return result;
}

Matrix Matrix::MultiplicationByValue(const Matrix &l, const Matrix &r) {
    Matrix result = Matrix();
    auto multiply = [&result](Matrix l, Matrix r, int i, int j) {
            result.values[i][j] = 0.0f;
            for(unsigned int k = 0; k < Matrix::SIZE; ++k) {
                result[i][j] += l[i][k] * r[k][j];
            }
    };

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    std::thread threads[Matrix::SIZE][Matrix::SIZE];
    // Start calculations
    for(unsigned int i = 0; i < Matrix::SIZE; ++i) {
        for(unsigned int j = 0; j < Matrix::SIZE; ++j) {
            threads[i][j] = std::thread(multiply, l, r, i, j);
        }
    }
    // Wait for threads to end
    for(unsigned int i = 0; i < Matrix::SIZE; ++i) {
        for(unsigned int j = 0; j < Matrix::SIZE; ++j) {
            threads[i][j].join();
        }
    }
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "By value matrix multiplication took \t\t" << std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count() << " nanoseconds\n";
    return result;
}

Matrix Matrix::MultiplicationByChunk(const Matrix &l, const Matrix &r) {
    Matrix result = Matrix();
    auto multiply = [&result](Matrix r, Matrix l, unsigned int i) {
        for(unsigned int j = 0; j < Matrix::SIZE; ++j) {
            result[i][j] = 0.0f;
            for(unsigned int k = 0; k < Matrix::SIZE; ++k) {
                result[i][j] += l[i][k] * r[k][j];
            }
        }
    };

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    std::thread threads[Matrix::SIZE];
    // Start calculations
    for(unsigned int i = 0; i < Matrix::SIZE; ++i) {
            threads[i] = std::thread(multiply, l, r, i);
    }
    // Wait for threads to end
    for(unsigned int i = 0; i < Matrix::SIZE; ++i) {
            threads[i].join();
    }
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "By chunk matrix multiplication took \t\t" << std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count() << " nanoseconds\n";
    return result;
}

float *Matrix::operator[](const int& i) {
    return this->values[i];
}

const float *Matrix::operator[](const int &i) const {
    return this->values[i];
}
