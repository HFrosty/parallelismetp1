#include "Matrix.h"
constexpr unsigned int TAB_SIZE = 4096;
constexpr unsigned int TAB_CHUNK_SIZE = 256;

void Exercice5() {
    auto monitor = [](std::mutex* mutex, std::condition_variable* condition, float* level, bool* purging) {
        while(1) {
            std::unique_lock<std::mutex> lock = std::unique_lock<std::mutex>(*mutex);
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            *level += 0.5f;
            std::cout << "Water level is now " << *level << std::endl;
            if(*level == 5.0f) {
                std::cout << "Water needs to be purged !" << std::endl;
                *purging = true;
                condition->notify_one();
                condition->wait(lock, [purging]{return !*purging;});
            }
        }
    };
    auto valve = [](std::mutex* mutex, std::condition_variable* condition, float* level, bool* purging) {
        while(1) {
            std::unique_lock<std::mutex> lock = std::unique_lock<std::mutex>(*mutex);
            condition->wait(lock, [purging]{return *purging;});
            std::cout << "Purging water..." << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            *level = 0.0f;
            std::cout << "Water purged" << std::endl;
            *purging = false;
            condition->notify_one();
        }
    };

    std::mutex mutex;
    std::condition_variable condition;
    float waterLevel = 0.0f;
    bool purging = false;
    std::thread t0 = std::thread(monitor, &mutex, &condition, &waterLevel, &purging);
    std::thread t1 = std::thread(valve, &mutex, &condition, &waterLevel, &purging);

    while(1) {

    }
}

void Exercice42() {
    std::mutex mutexOdd;
    std::mutex mutexEven;

    auto displayEven = [&mutexOdd, &mutexEven]() {
        for(unsigned int i = 0; i < 1000; ++i) {
            if(i % 2 == 0) {
                mutexEven.lock();
                std::cout << i << std::endl;
                mutexOdd.unlock();
            }
        }
    };
    auto displayOdd = [&mutexOdd, &mutexEven]() {
        for(unsigned int i = 0; i < 1000; ++i) {
            if(i % 2 == 1) {
                mutexOdd.lock();
                std::cout << i << std::endl;
                mutexEven.unlock();
            }
        }
    };

    mutexOdd.lock();
    // Even is already unlocked
    std::thread tEven = std::thread(displayEven);
    std::thread tOdd = std::thread(displayOdd);

    tEven.join();
    tOdd.join();
}

void Exercice41() {
    auto displayEven = []() {
        for(unsigned int i = 0; i < 1000; ++i) {
            if(i % 2 == 0) {
                std::cout << i << std::endl;
            }
        }
    };
    auto displayOdd = []() {
        for(unsigned int i = 0; i < 1000; ++i) {
            if(i % 2 == 1) {
                std::cout << i << std::endl;
            }
        }
    };
    std::thread tEven = std::thread(displayEven);
    std::thread tOdd = std::thread(displayOdd);

    tEven.join();
    tOdd.join();
}

void Exercice33() {
    // Thread function
    auto CalculateSum = [](const float* t, const unsigned int& start, float* sum) {
        const unsigned int limit = start + TAB_CHUNK_SIZE;
        for(unsigned int i = start; i < limit; ++i) {
            *sum += t[i];
        }
    };

    // Generate random numbers between 0 and 10
    float t[TAB_SIZE];
    for(unsigned int i = 0; i < TAB_SIZE; ++i) {
        t[i] = static_cast <float> (std::rand()) / (static_cast <float> (RAND_MAX/100.0f));
    }
    // Calculate sum
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    float sum = 0.0f;
    std::thread threads[TAB_SIZE / TAB_CHUNK_SIZE];
    for(unsigned int i = 0; i < TAB_SIZE / TAB_CHUNK_SIZE; ++i) {
        threads[i] = std::thread(CalculateSum, t, i * TAB_CHUNK_SIZE, &sum);
    }
    // Wait for threads to end
    for(unsigned int i = 0; i < TAB_SIZE / TAB_CHUNK_SIZE; ++i) {
        threads[i].join();
    }
    // Measure elapsed time
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Chunk with shared variable sum calculation took " << std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count() << " nanoseconds\n";
}

void Exercice32() {
    // Thread function
    auto CalculateSum = [](const float* t, const unsigned int& start) {
        float sum = 0.0f;
        const unsigned int limit = start + TAB_CHUNK_SIZE;
        for(unsigned int i = start; i < limit; ++i) {
            sum += t[i];
        }
    };

    // Generate random numbers between 0 and 10
    float t[TAB_SIZE];
    for(unsigned int i = 0; i < TAB_SIZE; ++i) {
        t[i] = static_cast <float> (std::rand()) / (static_cast <float> (RAND_MAX/100.0f));
    }
    // Calculate sum
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    std::thread threads[TAB_SIZE / TAB_CHUNK_SIZE];
    for(unsigned int i = 0; i < TAB_SIZE / TAB_CHUNK_SIZE; ++i) {
        threads[i] = std::thread(CalculateSum, t, i * TAB_CHUNK_SIZE);
    }
    // Wait for threads to end
    for(unsigned int i = 0; i < TAB_SIZE / TAB_CHUNK_SIZE; ++i) {
        threads[i].join();
    }
    // Measure elapsed time
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Chunk with local variable sum calculation took " << std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count() << " nanoseconds\n";
}

void Exercice31() {
    // Generate random numbers between 0 and 10
    float t[TAB_SIZE];
    for(unsigned int i = 0; i < TAB_SIZE; ++i) {
        t[i] = static_cast <float> (std::rand()) / (static_cast <float> (RAND_MAX/100.0f));
    }
    // Calculate sum
    float sum = 0.0f;
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    for(unsigned int i = 0; i < TAB_SIZE; ++i) {
        sum += t[i];
    }
    // Measure elapsed time
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Sequential sum calculation took " << std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count() << " nanoseconds\n";
}

void Exercice2() {
    Matrix m1; Matrix m2;
    Matrix::MultiplicationSequential(m1, m2);
    Matrix::MultiplicationByValue(m1, m2);
    Matrix::MultiplicationByChunk(m1, m2);
}

void Exercice1() {
    auto displayHelloWorld = []() -> void {
        std::cout << "Hello World" << std::endl;
    };
    std::thread t0 = std::thread(displayHelloWorld);
    std::thread t1 = std::thread(displayHelloWorld);
    t0.join();
    t1.join();
}

int main() {
    //Exercice1();
    //Exercice2();
    //Exercice31();
    //Exercice32();
    //Exercice33();
    //Exercice41();
    //Exercice42();
    Exercice5();
    return 0;
}
