//
// Created by AURELIEN on 28/09/2020.
//

#ifndef PARALLELISMETP1_MATRIX_H
#define PARALLELISMETP1_MATRIX_H

#include "includes.h"

class Matrix {
public:
    Matrix();

    static Matrix MultiplicationSequential(const Matrix &l, const Matrix &r);
    static Matrix MultiplicationByValue(const Matrix &l, const Matrix &r);
    static Matrix MultiplicationByChunk(const Matrix &l, const Matrix &r);

    float* operator[](const int& i);
    const float* operator[](const int& i) const;

private:
    static constexpr unsigned int SIZE = 64;
    float values[SIZE][SIZE];
};


#endif //PARALLELISMETP1_MATRIX_H
